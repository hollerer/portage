# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module
DESCRIPTION="Formats markdown and launches it in a browser."
HOMEPAGE="https://github.com/mapitman/mdview"


EGO_SUM=(
	"github.com/pkg/browser v0.0.0-20180916011732-0a3d74bf9ce4"
	"github.com/pkg/browser v0.0.0-20180916011732-0a3d74bf9ce4/go.mod"
	"github.com/russross/blackfriday v2.0.0+incompatible"
	"github.com/russross/blackfriday v2.0.0+incompatible/go.mod"
	"github.com/shurcooL/sanitized_anchor_name v1.0.0"
	"github.com/shurcooL/sanitized_anchor_name v1.0.0/go.mod"
	"gitlab.com/golang-commonmark/html v0.0.0-20191124015941-a22733972181"
	"gitlab.com/golang-commonmark/html v0.0.0-20191124015941-a22733972181/go.mod"
	"gitlab.com/golang-commonmark/linkify v0.0.0-20191026162114-a0c2df6c8f82"
	"gitlab.com/golang-commonmark/linkify v0.0.0-20191026162114-a0c2df6c8f82/go.mod"
	"gitlab.com/golang-commonmark/markdown v0.0.0-20191127184510-91b5b3c99c19"
	"gitlab.com/golang-commonmark/markdown v0.0.0-20191127184510-91b5b3c99c19/go.mod"
	"gitlab.com/golang-commonmark/mdurl v0.0.0-20191124015652-932350d1cb84"
	"gitlab.com/golang-commonmark/mdurl v0.0.0-20191124015652-932350d1cb84/go.mod"
	"gitlab.com/golang-commonmark/puny v0.0.0-20191124015043-9f83538fa04f"
	"gitlab.com/golang-commonmark/puny v0.0.0-20191124015043-9f83538fa04f/go.mod"
	"gitlab.com/opennota/wd v0.0.0-20180912061657-c5d65f63c638"
	"gitlab.com/opennota/wd v0.0.0-20180912061657-c5d65f63c638/go.mod"
	"golang.org/x/text v0.3.2"
	"golang.org/x/text v0.3.2/go.mod"
	"golang.org/x/tools v0.0.0-20180917221912-90fa682c2a6e/go.mod"
)

go-module_set_globals

SRC_URI="https://github.com/mapitman/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
	${EGO_SUM_SRC_URI}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

src_compile() {
	go build || die "compile failed"
}

src_install() {
	dobin mdview
}
