# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="An extremely fast and simple dmenu / rofi replacement for wlroots-based Wayland compositors such as Sway."
HOMEPAGE="https://github.com/philj56/tofi"

inherit meson
inherit git-r3
EGIT_REPO_URI="https://github.com/philj56/tofi"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

PATCHES=(
	"${FILESDIR}/alt-next-result.patch"
)

RDEPEND="
	dev-libs/wayland
	media-libs/freetype
	media-libs/harfbuzz
	x11-libs/cairo
	x11-libs/libxkbcommon
	x11-libs/pango
"

BDEPENDS="
	app-text/scdoc
	dev-libs/wayland-protocols
	dev-util/meson
"
