# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Small GTK GUI application to set contrast, brightness and gamma for wayland compositors which support the wlr-gamma-control protocol extension."
HOMEPAGE="https://github.com/mischw/wl-gammactl"

inherit meson
inherit git-r3
EGIT_REPO_URI="https://github.com/mischw/wl-gammactl"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

BDEPEND="
	dev-util/meson
"
