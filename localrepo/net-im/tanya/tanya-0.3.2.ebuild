# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module
DESCRIPTION="slack irc gateway in a world with no generics"
HOMEPAGE="https://github.com/nolanlum/tanya"


EGO_SUM=(
	"github.com/BurntSushi/toml v0.3.1"
	"github.com/BurntSushi/toml v0.3.1/go.mod"
	"github.com/davecgh/go-spew v1.1.1"
	"github.com/davecgh/go-spew v1.1.1/go.mod"
	"github.com/go-test/deep v1.0.4"
	"github.com/go-test/deep v1.0.4/go.mod"
	"github.com/gorilla/websocket v1.4.2"
	"github.com/gorilla/websocket v1.4.2/go.mod"
	"github.com/pkg/errors v0.8.0"
	"github.com/pkg/errors v0.8.0/go.mod"
	"github.com/pmezard/go-difflib v1.0.0"
	"github.com/pmezard/go-difflib v1.0.0/go.mod"
	"github.com/slack-go/slack v0.10.0"
	"github.com/slack-go/slack v0.10.0/go.mod"
	"github.com/stretchr/testify v1.2.2"
	"github.com/stretchr/testify v1.2.2/go.mod"
	"golang.org/x/sys v0.0.0-20201119102817-f84b799fce68"
	"golang.org/x/sys v0.0.0-20201119102817-f84b799fce68/go.mod"
	"golang.org/x/term v0.0.0-20210220032956-6a3ed077a48d"
	"golang.org/x/term v0.0.0-20210220032956-6a3ed077a48d/go.mod"
)

go-module_set_globals

SRC_URI="https://github.com/nolanlum/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	${EGO_SUM_SRC_URI}"

LICENSE="BSD-4"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

src_compile() {
	go build || die "compile failed"
}

src_install() {
	dobin tanya

	insinto /usr/share/${P}
	doins config.toml.example
}

pkg_postinst() {
	einfo "An example configuration file can be found at '/usr/share/${P}'."
}
